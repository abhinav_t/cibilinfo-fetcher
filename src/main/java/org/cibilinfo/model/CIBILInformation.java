package org.cibilinfo.model;

import java.util.Objects;

public class CIBILInformation {

    public final String name;
    public final String panCardNumber;
    public final Integer cibilScore;

    public CIBILInformation(String name, String panCardNumber, Integer cibilScore) {
        this.name = name;
        this.panCardNumber = panCardNumber;
        this.cibilScore = cibilScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CIBILInformation that = (CIBILInformation) o;
        return name.equals(that.name) &&
                panCardNumber.equals(that.panCardNumber) &&
                cibilScore.equals(that.cibilScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, panCardNumber, cibilScore);
    }

    @Override
    public String toString() {
        return "CIBILInformation{" +
                "name='" + name + '\'' +
                ", panCardNumber='" + panCardNumber + '\'' +
                ", cibilScore=" + cibilScore +
                '}';
    }

}

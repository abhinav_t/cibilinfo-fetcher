package org.cibilinfo.fetcher;

import org.cibilinfo.model.CIBILInformation;

public class CIBILInformationFetcher {

    public CIBILInformation getCIBILInformation(String customerId) {
        return mockCIBILInformation();
    }

    private CIBILInformation mockCIBILInformation() {
        return new CIBILInformation("John Doe", "ABCDE1234F", 700);
    }

}
